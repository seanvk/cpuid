#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#define _GLIBCXX_USE_CXX11_ABI 1
using namespace std;

int
main ()
{
  string line;
  ifstream finfo ("/proc/cpuinfo");
  while (getline (finfo, line))
    {
      stringstream str (line);
      string itype;
      string info;
      if (getline (str, itype, ':') && getline (str, info)
          && itype.substr (0, 10) == "model name")
        {
          cout << info << endl;
          break;
        }
    }
}

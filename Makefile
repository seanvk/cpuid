CC=gcc
CXX=g++
RM=rm -f

CFLAGS += -g -Wall

SRCS=cpu-string.cc getpsn.c brandstr-regrd.c cpuid.c
OBJS=cpu-string.o getpsn.o brandstr-regrd.o cpuid.o
BINS=cpu-string getpsn brandstr-regrd cpuid

all: cpu-string getpsn brandstr-regrd cpuid

cpu-string: cpu-string.o
	$(CXX) -o cpu-string cpu-string.o

getpsn: getpsn.o
	$(CXX) -o getpsn getpsn.o

brandstr-regrd: brandstr-regrd.o
	$(CC) -o brandstr-regrd brandstr-regrd.o

cpuid: cpuid.o
	$(CC) -o cpuid cpuid.o

.depend: $(SRCS)
	$(RM) ./.depend
	$(CXX) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS)

distclean: clean
	$(RM) $(BINS) *~ .depend

include .depend
